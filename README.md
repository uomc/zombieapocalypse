# ZombieApocalypse

A zombie apocalypse game mode for Spigot, where players fight to be the last zombie remaining in increasingly difficult waves of zombies. Players that die become zombies and have to kill other players.

## Commands

- `/z begin`  starts the event, people will get teleported to the main map and looting phase begins
- `/z start` starts the apocalypse, zombies will start spawning
- `/z cure <player>` will turn a zombie player to human
- `/z zombify <player>` will turn a human player into a zombie
- `/z progress` increases spawn rates by 30%, useful if players are too good

## Dependencies

- [Lib's Diguises](https://www.spigotmc.org/resources/libs-disguises-free.81/)
- [ProtocolLib](https://www.spigotmc.org/resources/protocollib.1997/)
- [com.github.EricLangezaal.ZombieApocalypse](https://www.spigotmc.org/resources/zombieapocalypse-8-zombie-types-1-20-support.82106/)

## Building

Ensure that maven and jdk>=8 are installed. Run `maven clean package`. Plugin file will be in `target/`

## Licensing

view [LICENSE](./LICENSE) for more information

&copy; 2023 University of Manchester Minecraft Society
