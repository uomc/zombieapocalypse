package xyz.davidovski.mc.zombies;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import java.util.stream.IntStream;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.loot.LootContext;
import org.bukkit.loot.LootTable;
import org.bukkit.plugin.Plugin;

// this is actually the loot table for chests that will spawn
public class ZombieLootTable implements LootTable {
    public NamespacedKey key;

    public ZombieLootTable(Plugin pl) {
        key = new NamespacedKey(pl, "zombietable");
    }

    @Override
    public NamespacedKey getKey() {
        return key;
    }

    @Override
    public void fillInventory(Inventory inventory, Random random, LootContext context) {
        Collection<ItemStack> items = populateLoot(random, context);
        items.forEach(item -> inventory.addItem(item));
        inventory.setItem(0, new ItemStack(Material.TNT));
    }

    @Override
    public Collection<ItemStack> populateLoot(Random random, LootContext context) {
        Collection<ItemStack> items = new ArrayList<ItemStack>();
        IntStream.range(0, random.nextInt()).forEach(i -> items.add(getFoodItems(random)));
        items.add(new ItemStack(Material.TNT));
        return items;
    }

    public ItemStack getFoodItems(Random random) {
        Material m = Material.GOLDEN_APPLE;
        double n = random.nextDouble();
        // these if else trees really arent a very good way of doing this but it will work

        if (n < 0.3) {
            m = Material.CARROTS;
        } else if (n < 0.5) {
            m = Material.COOKED_CHICKEN;
        } else if (n < 0.7) {
            m = Material.COOKED_BEEF;
        } else if (n < 0.9) {
            m = Material.COOKED_COD;
        } else if (n < 0.95) {
            m = Material.COOKED_PORKCHOP;
        }

        return new ItemStack(m, random.nextInt(7));
    }

    public ItemStack getToolItems(Random random) {
        String material = "";
        String tool = "";

        int toolnumber = random.nextInt(8);

        // lol
        boolean isArmour = (toolnumber > 3);
        material = isArmour ? "leather" : "wooden";
        if (random.nextBoolean()) {
            material = isArmour ? "stone" : "";
            if (random.nextBoolean()) {
                material = "iron";
                if (random.nextBoolean()) {
                    material = "diamond";
                }
            }
        }


        switch (toolnumber) {
            case 0:
                tool = "axe";
                break;
            case 1:
                tool = "sword";
                break;
            case 2:
                tool = "pickaxe";
                break;
            case 3:
                tool = "shovel";
                break;
            case 4:
                tool = "chestplate";
                break;
            case 5:
                tool = "leggings";
                break;
            case 6:
                tool = "boots";
                break;
            case 7:
                tool = "helmet";
                break;
        }

        return new ItemStack(Material.getMaterial(material + "_" + tool), random.nextInt(7));
    }
}
