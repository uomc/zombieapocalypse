package xyz.davidovski.mc.zombies;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.GameRule;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;
import org.bukkit.entity.SpawnCategory;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.loot.Lootable;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.ericdebouwer.zombieapocalypse.api.Apocalypse;
import com.ericdebouwer.zombieapocalypse.api.ApocalypseAPI;

import me.libraryaddict.disguise.DisguiseConfig.NotifyBar;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.FlagWatcher;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import xyz.davidovski.mc.zombies.commands.ZombiesCommand;

public class ZombiesPlugin extends JavaPlugin implements Listener {

    public static long GAME_DURATION_SECONDS = 45*60;

	private ApocalypseAPI apocalypseAPI;

    public Map<UUID, MobDisguise> zombies;

    private boolean glowing = false;

    public void setGlowing(boolean glowing) {
        this.glowing = glowing;
    }

    public boolean getGlowing() {
        return this.glowing;
    }

    public ZombiesPlugin() {
        apocalypseAPI = ApocalypseAPI.getInstance();
        zombies = new HashMap<>();
    }

    public boolean isApocalypse(World world) {
        return apocalypseAPI.isApocalypse("" + world.getName());
    }

	public boolean start(World world) {
        if (isApocalypse(world))
            return false;

        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        world.setGameRule(GameRule.DO_INSOMNIA, false);
        world.setGameRule(GameRule.KEEP_INVENTORY, true);

        world.setDifficulty(Difficulty.EASY);
        world.setTime(18500);

		boolean started = apocalypseAPI.startApocalypse("" + world.getName(), 60*24, Bukkit.getSpawnLimit(SpawnCategory.MONSTER) / 2, false);

        Apocalypse apocalypse = apocalypseAPI.getApocalypse("" + world.getName());
        if (apocalypse == null)
            return false;

        apocalypse.getBossBar().removeAll();

        world.getPlayers().forEach(player -> player.sendTitle(ChatColor.DARK_GREEN + "Armageddon is approaching...", "", 1, 35, 1));
        world.getPlayers().forEach(player -> player.playSound(player.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1.0f, 1.0f));

        world.getPlayers().forEach(player -> {
            player.sendMessage(ChatColor.GRAY + "Armageddon is approaching...");
            player.sendMessage(ChatColor.GRAY + "Be the last to survive the waves zombies");
            player.sendMessage(ChatColor.GRAY + "Good luck!");
        });
        world.getWorldBorder().setSize(420, 0);
        world.getWorldBorder().setSize(2, GAME_DURATION_SECONDS);
        return started;
	}

	public boolean stop(World world) {
        if (!isApocalypse(world))
            return false;

        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, true);
        world.setDifficulty(Difficulty.PEACEFUL);
        world.setTime(0);
        world.getWorldBorder().setSize(420, 0);
		return apocalypseAPI.endApocalypse("" + world.getName(), false);
	}

	public int progress(World world) {
        Apocalypse apocalypse = apocalypseAPI.getApocalypse("" + world.getName());
        if (apocalypse == null)
            return 0;
        int cap = (int) (apocalypse.getMobCap() * 1/3);
        apocalypseAPI.setMobCap("" + world.getName(), cap);
        return cap;
	}

	@Override
    public void onEnable() {
        getLogger().info("enabling plugin " +  this.getName());
        getServer().getPluginManager().registerEvents(this, this);

        this.getCommand("zombies").setExecutor(new ZombiesCommand(this));

        getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
            getServer().getWorlds().forEach(world -> {
                if (isApocalypse(world))
                    progress(world);

            });
        }, 0, 20L * 60L * 8L);

        getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
            getServer().getOnlinePlayers().forEach(player -> {
                player.setGlowing(glowing && !isZombie(player));

                if (!isZombie(player))
                    return;

                player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 100, 0, true));
                player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor.DARK_GREEN + "you are a zombie"));


            });
        }, 0, 20L);
    }

    @Override
    public void onDisable() {
        getLogger().info("disabling plugin " +  this.getName());

        // TODO dont do this, but save state between reloads
        this.getServer().getWorlds().forEach(world -> stop(world));
        this.getServer().getOnlinePlayers().forEach(player -> dezombifyPlayer(player, false));
    }

    public void updateDisguises(Player player) {
        MobDisguise mobDisguise = new MobDisguise(DisguiseType.ZOMBIE);
        mobDisguise.setEntity(player);
        mobDisguise.setSelfDisguiseVisible(true);
        mobDisguise.startDisguise();
        mobDisguise.setNotifyBar(NotifyBar.NONE);

        FlagWatcher watcher = mobDisguise.getWatcher();
        watcher.setCustomName(player.getName());

        zombies.put(player.getUniqueId(), mobDisguise);

        // make the player have a zombie helmet
        ItemStack head = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta meta = (SkullMeta) head.getItemMeta();
        meta.setOwnerProfile(player.getPlayerProfile());
        head.setItemMeta(meta);
        head.addEnchantment(Enchantment.BINDING_CURSE, 1);
        head.addEnchantment(Enchantment.VANISHING_CURSE, 1);
        player.getInventory().setHelmet(head);
    }

    public void unsetDisguises(Player player) {
        if (!zombies.containsKey(player.getUniqueId()))
            return;
        zombies.get(player.getUniqueId()).stopDisguise();
        zombies.remove(player.getUniqueId());
    }

    public void zombifyPlayer(Player player, boolean announce) {

        player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue());
        player.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(0.08);

        player.closeInventory();

        // drop the player's current helmet
        ItemStack helmet = player.getInventory().getHelmet();
        if (helmet != null && helmet.getType() != Material.PLAYER_HEAD)
            player.getWorld().dropItem(player.getLocation(), helmet);

        // stop zombies from targeting this player
        player.getWorld().getEntities().forEach(e -> {
            if (e instanceof Mob)
                ((Mob) e).setTarget(null);
        });

        updateDisguises(player);

        if (announce)
            announceZombification(player);
    }

    public void announceZombification(Player player) {
            player.getServer().broadcastMessage(ChatColor.RED + player.getName() + ChatColor.GOLD + " is now a zombie");
            player.sendMessage("You have turned into a " + ChatColor.DARK_GREEN + "zombie!");
            player.sendMessage("Kill humans and eat rotten flesh to survive");

            player.getWorld().getPlayers().forEach(p -> p.playSound(player.getLocation(), Sound.ENTITY_ZOMBIE_VILLAGER_CURE, 1.0f, 1.0f));
    }

    public void dezombifyPlayer(Player player, boolean announce) {
        unsetDisguises(player);

        player.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(0.1);
        if (announce)
            player.getServer().broadcastMessage(ChatColor.RED + player.getName() + ChatColor.GOLD + " is now a human");
    }

    public boolean isZombie(Player player) {
        return zombies.containsKey(player.getUniqueId());
    }

    @EventHandler
    public void onEntityDamageByEntitiy(EntityDamageByEntityEvent e) {
        if (!(e.getEntity() instanceof Player))
            return;
        Player p = (Player) e.getEntity();
        Entity damager = e.getDamager();
        // do not allow zombies to hurt each other
        if (damager != null && damager instanceof Player) {
            if (!(isZombie(p) ^ isZombie((Player) damager))) {
                e.setCancelled(true);
                return;
            }
        }


        // this might be redundant to repeat here
        if (isZombie(p))
            return;

        if (p.getHealth() - e.getFinalDamage() > 0)
            return;

        if (!isApocalypse(p.getWorld()))
            return;

        e.setCancelled(true);
        zombifyPlayer(p, true);
    }


    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player))
            return;
        Player p = (Player) e.getEntity();

        if (isZombie(p))
            return;

        if (p.getHealth() - e.getFinalDamage() > 0)
            return;

        if (!isApocalypse(p.getWorld()))
            return;

        e.setCancelled(true);
        zombifyPlayer(p, true);
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        Player p = e.getPlayer();
        if (isZombie(p))
            zombifyPlayer(p, false);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        if (isZombie(p))
            zombifyPlayer(p, false);
    }

    @EventHandler
    public void onMobTarget(EntityTargetLivingEntityEvent e) {
        if (!(e.getTarget() instanceof Player)) return;
        Player p = (Player) e.getTarget();

        if (isZombie(p)) 
            e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerEat(PlayerItemConsumeEvent e) {
        if (!isZombie(e.getPlayer()))
            return;

        if (e.getItem().getType() != Material.ROTTEN_FLESH) {
            e.setCancelled(true);
            e.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor.GRAY + "can't eat this item!"));
            return;
        }

        e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 60, 1));

        // TODO if being a zombie is too hard, then do this
        //e.getPlayer().setFoodLevel(e.getPlayer().getFoodLevel() + 3);
    }

    public void generateChest(Block block) {
        if (block.getType() != Material.CHEST)
            block.setType(Material.CHEST);

        Chest chest = (Chest) block.getState();

        if (!(chest instanceof Lootable))
            return;

        getServer().getLogger().info("populating loot at " + block.getLocation());

        //chest.setLootTable(new ZombieLootTable(this));
        chest.setLootTable(Bukkit.getLootTable(NamespacedKey.fromString("zombie:loots")));
        chest.update();
    }

    // this is confusing but this is when the grace period starts not the apocalypse
    public void begin(World world) {
        Location spawn = new Location(world, 624.0d, 67.0d, 522.0d);
        world.setSpawnLocation(spawn);
        world.getWorldBorder().setSize(420);
        world.getPlayers().forEach(p -> {
            p.setGameMode(GameMode.SURVIVAL);
            p.setHealth(p.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
            p.teleport(spawn);

            p.sendMessage(ChatColor.GRAY + "The goal is to survive the armageddon for as long as possible!");
            p.sendMessage(ChatColor.GRAY + "Loot chests, make bunkers, team up with others to give yourself the best chances.");
            p.sendMessage(ChatColor.GRAY + "When you die, you are converted into a zombie and have to find and elimitate the remaining survivors.");
            p.sendMessage(ChatColor.GRAY + "You have a couple of minutes before it approaches...");
            p.sendMessage(ChatColor.DARK_GREEN + "Good luck!");
        });

    }
}
