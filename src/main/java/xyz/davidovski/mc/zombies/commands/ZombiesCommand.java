package xyz.davidovski.mc.zombies.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import xyz.davidovski.mc.zombies.ZombiesPlugin;

public class ZombiesCommand implements CommandExecutor {

    private ZombiesPlugin plugin;

    public ZombiesCommand(ZombiesPlugin plugin) {
        this.plugin = plugin;
	}

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        if (!sender.isOp())
            return false;

        if (args.length < 1) 
            return false;

        Player player = (Player) sender;

        switch (args[0]) {
            case "start":
                if (!plugin.start(((Player) sender).getWorld()))
                    return false;
                sender.sendMessage(ChatColor.GOLD + "apocalypse has started");
                break;
            case "stop":
                if (!plugin.stop(((Player) sender).getWorld()))
                    return false;
                sender.sendMessage(ChatColor.GOLD + "apocalypse has ended");
                break;
            case "progress":
                int cap = plugin.progress(((Player) sender).getWorld());
                if (cap == 0)
                    return false;

                sender.sendMessage(ChatColor.GOLD + "apocalypse is now at " + ChatColor.RED + cap);
                break;
            case "zombify":
                if (args.length > 1)
                    player = Bukkit.getPlayer(args[1]);

                if (player == null)
                    return false;

                plugin.zombifyPlayer(player, true);
                break;

            case "cure":
                if (args.length > 1)
                    player = Bukkit.getPlayer(args[1]);

                if (player == null)
                    return false;

                plugin.dezombifyPlayer(player, true);
                break;

            case "chest":
                plugin.generateChest(player.getLocation().getBlock());
                break;
            case "begin":
                plugin.begin(player.getWorld());
                break;
            case "glow":
                plugin.setGlowing(!plugin.getGlowing());
                break;
        }

        return true;

    }
}
